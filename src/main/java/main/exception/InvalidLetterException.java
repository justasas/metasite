package main.exception;

/**
 * Created by justas.rutkauskas on 7/27/2017.
 */
public class InvalidLetterException extends ApplicationException {
    public InvalidLetterException(String message) {
        super(message);
    }
    public InvalidLetterException() {
        super();
    }
}
