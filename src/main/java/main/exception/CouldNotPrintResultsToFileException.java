package main.exception;

/**
 * Created by justas.rutkauskas on 7/26/2017.
 */
public class CouldNotPrintResultsToFileException extends ApplicationException{
    public CouldNotPrintResultsToFileException(String message) {
        super(message);
    }
}
