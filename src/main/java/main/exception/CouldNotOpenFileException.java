package main.exception;

/**
 * Created by justas.rutkauskas on 7/25/2017.
 */
public class CouldNotOpenFileException extends ApplicationException {
    public CouldNotOpenFileException(String message) {
        super(message);
    }
}
