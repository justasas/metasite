package main.exception;

/**
 * Created by justas.rutkauskas on 7/25/2017.
 */
public class ApplicationException extends Exception {
    ApplicationException(String message)
    {
        super(message);
    }

    public ApplicationException() {
        super();
    }
}
