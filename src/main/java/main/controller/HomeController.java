package main.controller;

import main.config.properties.ApplicationProperties;
import main.exception.InvalidLetterException;
import main.model.service.Main;
import main.repository.WordsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Controller
public class HomeController {

    @Autowired
    private Main main;

    @Autowired
    private WordsRepository wordsRepository;

    @Autowired
    private ApplicationProperties applicationProperties;

    @RequestMapping("/")
    public String uploading(Model model) {
        return "index";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String uploadingPost(@RequestParam("uploadingFiles") MultipartFile[] uploadingFiles, Model model) throws IOException, InterruptedException, InvalidLetterException {

        main.countFrequencyOfEachWordFromFiles(convertMultipartToFile(uploadingFiles));

        model.addAttribute("dividedWords", main.divideWordsToAlphabeticalRanges(wordsRepository.getAllWordsWithCount()));

        return "index";
    }

//    private String[] convertMultipartToFile(MultipartFile[] uploadingFiles) throws IOException {
//        String[] pathToFile = new String[uploadingFiles.length];
//
//        int c = 0;
//        for(MultipartFile uploadedFile : uploadingFiles) {
//            File file = new File(uploadedFile.getOriginalFilename());
//            uploadedFile.transferTo(file);
//            pathToFile[c++] = file.getAbsolutePath();
//        }
//
//        return pathToFile;
//    }

    public String[] convertMultipartToFile(MultipartFile[] uploadingFiles) throws IOException {

        String[] pathToFile = new String[uploadingFiles.length];

        int c = 0;

        for(MultipartFile uploadedFile : uploadingFiles) {
            File convFile = new File(applicationProperties.getUploadFilesFolder() + File.separator + uploadedFile.getOriginalFilename());
            convFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(uploadedFile.getBytes());
            fos.close();

            pathToFile[c++] = convFile.getAbsolutePath();
        }
        return pathToFile;
    }
}