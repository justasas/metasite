package main;

import main.model.service.Main;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication()
@EnableAutoConfiguration
public class App extends SpringBootServletInitializer implements CommandLineRunner {

    @Autowired
    private Main main;

    public static void main(String[] args) {
        new SpringApplicationBuilder().sources(App.class).run(args);
    }

    @Override
    public void run(String... args) throws InterruptedException {
        if(args.length != 0)
            main.start(args);
    }

}
