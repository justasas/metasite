package main.model;

import main.exception.CouldNotOpenFileException;
import main.repository.WordsRepository;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
@Scope("prototype")
public class WordsFrequencyCountingThread extends Thread {

    private String filePath;

    @Autowired
    private WordsRepository wordsRepository;

    static private Object lock = new Object();

    WordsFrequencyCountingThread(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public void run() {

        try {
            processFile();
        } catch (CouldNotOpenFileException couldNotOpenFile) {
            System.out.println(couldNotOpenFile.getMessage());
        }
    }

    private void processFile() throws CouldNotOpenFileException {
        LineIterator it = getLineIterator(filePath);
        try {
            while (it.hasNext()) {
                String line = it.nextLine();
                addWords(line);
            }
        } finally {
            LineIterator.closeQuietly(it);
        }
    }

    private void addWords(String line) {
        String[] lineWords = line.split(" ");
        for (String word : lineWords) {
            synchronized (lock) {
                String lowerCaseWord = word.toLowerCase();
                if (wordsRepository.isWordExist(lowerCaseWord))
                    wordsRepository.increaseExistingWordCount(lowerCaseWord);
                else
                    wordsRepository.insertNew(lowerCaseWord);
            }
        }
    }

    private LineIterator getLineIterator(String fileLocation) throws CouldNotOpenFileException {
        LineIterator it;
        try {
            it = FileUtils.lineIterator(new File(fileLocation), "UTF-8");
        } catch (IOException e) {
            throw (new CouldNotOpenFileException("File: " + fileLocation + "does not exist or could not be opened"));
        }
        return it;
    }


}
