package main.model.service;

import main.exception.ApplicationException;
import main.exception.InvalidLetterException;
import main.model.service.caption.CaptionService;
import main.repository.WordsRepository;
import main.model.AlphabeticRangesToDivideResultsEnum;
import main.model.WordsFrequencyCountingThread;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class Main {

    @Autowired
    private ApplicationContext appCtx;

    @Autowired
    private WordsRepository wordsRepository;

    @Autowired
    private PrintingService printingService;

    @Autowired
    private CaptionService captionService;

    private static Logger log = Logger.getLogger(Main.class.getName());

    public void start(String[] pathsToFilesContainingEnglishWords) throws InterruptedException {

        try {
            countFrequencyOfEachWordFromFiles(pathsToFilesContainingEnglishWords);
            printingService.printWordsWithCountToFiles(divideWordsToAlphabeticalRanges(wordsRepository.getAllWordsWithCount()));
        } catch (ApplicationException appEx)
        {
            System.out.println(appEx.getMessage());
        }
        System.exit(0);
    }

    public void countFrequencyOfEachWordFromFiles(String[] pathsToFilesContainingEnglishWords) throws InterruptedException {
        log.info("Starting to count frequency of each word from all files...");

        List<WordsFrequencyCountingThread> allThreads = new ArrayList<WordsFrequencyCountingThread>();

        for (String fileLocation : pathsToFilesContainingEnglishWords) {
            WordsFrequencyCountingThread wordsFrequencyCountingThread = (WordsFrequencyCountingThread) appCtx.getBean("wordsFrequencyCountingThread", fileLocation);
            wordsFrequencyCountingThread.start();

            allThreads.add(wordsFrequencyCountingThread);
        }

        for (Thread thread : allThreads) {
            thread.join();
        }

        log.info("Ended to count frequency of each word from all files.");
    }

    public Map<AlphabeticRangesToDivideResultsEnum, Map<String, Integer>> divideWordsToAlphabeticalRanges(Map<String, Integer> wordsWithCount) throws InvalidLetterException {
        log.info("Started to divide words to alphabetical ranges...");

        Map<AlphabeticRangesToDivideResultsEnum, Map<String, Integer>> ret = new HashMap<AlphabeticRangesToDivideResultsEnum, Map<String, Integer>>();

        initMap(ret);

        for (Map.Entry<String, Integer> wordWithCount : wordsWithCount.entrySet()) {
            char firstLetter = wordWithCount.getKey().charAt(0);

            ret.get(AlphabeticRangesToDivideResultsEnum.getRange(firstLetter)).put(wordWithCount.getKey(), wordWithCount.getValue());
        }

        log.info("Ended to divide words to alphabetical ranges.");

        return ret;
    }

    private void initMap(Map<AlphabeticRangesToDivideResultsEnum, Map<String, Integer>> ret) {
        for(AlphabeticRangesToDivideResultsEnum range : AlphabeticRangesToDivideResultsEnum.values())
        {
            ret.put(range, new HashMap<String, Integer>());
        }
    }
}
