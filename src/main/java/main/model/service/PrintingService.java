package main.model.service;

import main.exception.CouldNotPrintResultsToFileException;
import main.model.AlphabeticRangesToDivideResultsEnum;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Map;

@Service
public class PrintingService {

    private static Logger log = Logger.getLogger(PrintingService.class.getName());
    private static final String RESULTS_DIRECTORY = "results";

    public void printWordsWithCountToConsole(Map<String, Integer> wordsWithCount) {
        for (Map.Entry<String, Integer> wordWithCount : wordsWithCount.entrySet()) {
            System.out.printf("%-50s : %d\n", wordWithCount.getKey(), wordWithCount.getValue());
        }
    }

    public void printWordsWithCountToFiles(Map<AlphabeticRangesToDivideResultsEnum, Map<String, Integer>> rangesToWords) throws CouldNotPrintResultsToFileException {
        log.info("Starting to print words to files...");

        for (Map.Entry<AlphabeticRangesToDivideResultsEnum, Map<String, Integer>> rangeToWordsWithCountInfo : rangesToWords.entrySet()) {
            String newFileName = RESULTS_DIRECTORY + File.separator + rangeToWordsWithCountInfo.getKey().name() + ".txt";
            printWordsToNewFile(rangeToWordsWithCountInfo.getValue(), newFileName);
        }

        log.info("Ended to print words to files created in: " + RESULTS_DIRECTORY + " directory");
    }

    private void printWordsToNewFile(Map<String, Integer> wordsWithCount, String pathName) throws CouldNotPrintResultsToFileException {

        File file = new File(pathName);

        try {
            file.createNewFile();

            FileOutputStream fos = new FileOutputStream(file);

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

            for (Map.Entry<String, Integer> wordToCountEntry : wordsWithCount.entrySet()) {
                bw.write(wordToCountEntry.getKey() + " " + wordToCountEntry.getValue());
                bw.newLine();
            }

            bw.close();
        } catch (IOException e){
            throw (new CouldNotPrintResultsToFileException("could not create file or print results to created file: " + pathName));
        }
    }
}
