package main.model.service.caption;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

@Service
public class DefaultCaptionService implements CaptionService {

    @Autowired
    private MessageSource messageSource;

    @Override
    public String getCaption(String key) {
        return messageSource.getMessage(key, null, LocaleContextHolder.getLocale());
    }

    @Override
    public String getCaption(String key, String ... args) {
        return messageSource.getMessage(key, args, LocaleContextHolder.getLocale());
    }
}
