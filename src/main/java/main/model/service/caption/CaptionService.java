package main.model.service.caption;

public interface CaptionService {
    String getCaption(final String key);
    String getCaption(final String key, String ... args);
}
