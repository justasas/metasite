package main.model;

import main.exception.InvalidLetterException;

public enum AlphabeticRangesToDivideResultsEnum {

    AG('a', 'g'), HN('h', 'n'), OU('o', 'u'), VZ('v', 'z');

    private char lowerBoundLetter, upperBoundLetter;

    AlphabeticRangesToDivideResultsEnum(char lowerBoundLetter, char upperBoundLetter)
    {
        this.lowerBoundLetter = lowerBoundLetter;
        this.upperBoundLetter = upperBoundLetter;
    }

    public char getLowerBoundLetter() {
        return lowerBoundLetter;
    }


    public char getUpperBoundLetter() {
        return upperBoundLetter;
    }

    public static AlphabeticRangesToDivideResultsEnum getRange(char letter) throws InvalidLetterException {
        for(AlphabeticRangesToDivideResultsEnum range : values())
        {
            if(isLetterInRange(letter, range))
            {
                return range;
            }
        }

        throw new InvalidLetterException("word first letter is not from english alphabet, letter is: " + letter);
    }


    private static boolean isLetterInRange(char firstLetter, AlphabeticRangesToDivideResultsEnum range) {
        return firstLetter >= range.getLowerBoundLetter() && firstLetter <= range.getUpperBoundLetter();
    }
}
