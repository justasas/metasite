package main.config;

import main.repository.InMemoryWordsRepository;
import main.repository.WordsRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

    @Bean
    public WordsRepository wordsRepository() {
        return new InMemoryWordsRepository();
    }


}