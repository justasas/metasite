package main.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "application")
@Component
public class ApplicationProperties {
    private String uploadFilesFolder;

    public String getUploadFilesFolder() {
        return uploadFilesFolder;
    }

    public void setUploadFilesFolder(String uploadFilesFolder) {
        this.uploadFilesFolder = uploadFilesFolder;
    }

}