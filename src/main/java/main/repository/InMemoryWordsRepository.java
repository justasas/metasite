package main.repository;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class InMemoryWordsRepository implements WordsRepository {

    Map<String, Integer> wordWithCount = new HashMap();

    @Override
    public void insertNew(String word) {
        wordWithCount.put(word, 1);
    }

    @Override
    public void increaseExistingWordCount(String word) {
        Integer countOfWordsFound = wordWithCount.get(word);
        wordWithCount.put(word, countOfWordsFound+1);
    }

    @Override
    public boolean isWordExist(String word) {
        return wordWithCount.get(word) != null ? true : false;
    }

    @Override
    public Map<String, Integer> getAllWordsWithCount() {
        return wordWithCount;
    }

}
