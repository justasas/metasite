package main.repository;

import java.util.Map;

public interface WordsRepository {
    void insertNew(String word);
    void increaseExistingWordCount(String word);
    boolean isWordExist(String word);
    Map<String, Integer> getAllWordsWithCount();
}
